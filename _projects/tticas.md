---
slug: tticas
categories: coding
date: 2019-02-25

# what displays in the portfolio grid
position: 3
caption:
  title: T-Ticas
  subtitle: clothing store
  thumbnail: /assets/img/projects/tticas/thumbnail.png

# main content
title: Development of widgets for store

slider:
  - thumbnail: /assets/img/projects/tticas/whatsapp-contact.png
    title: contact info
  - thumbnail: /assets/img/projects/tticas/whatsapp-widget.png
    title: Whatsapp widget

timeline:
    section: timeline
    # left is the default
    #start_align: "left"
    events:
      - title: "Where it all started"
        year: "Feb 2019"
        desc: "We were first approached on [99Freelas](https://www.99freelas.com.br/user/jlammer){:target='_blank'} platform, where we had our first briefing."
        image: 
          src: /assets/img/projects/tticas/start.png
        alt: 
      - title: "Meeting"
        year: "Feb 2019"
        desc: "Right after we had most stuff settled using 99Freelas chat, we decided to sit and have a conversation in order to put details into paper."
        image: 
          src: /assets/img/general/meeting.png
        alt: 
      - title: "Design"
        year: "Feb 2019"
        desc: "Once we knew we were at the same page, we started working by designing widgets that would later be implemented in Wordpress."
        image: 
          src: /assets/img/projects/tticas/facebook.png
        alt: image alt text
      - title: "Development"
        year: "Feb 2019"
        desc: "After that, it was just a matter of implementing the existing **responsive design** into code."
        image: 
          src: /assets/img/general/dev.png
        alt: 
      - title: "Payment"
        year: "Feb 2019"
        desc: "After everything was delivered, we were paid by the client."
        image: 
          src: /assets/img/general/review.jpg
        alt: 
    end: "Be Part <br> of Our <br> Story!"
---

{:.section-subheading.text-muted}
Our customer from **T-Ticas** requested services for development of many different widgets for their store. Everything worked out nicely and **within deadline**, and in the end we had an enhanced Wordpress store ready to earn more money.

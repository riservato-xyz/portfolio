---
layout: default
title: Meeting
background: grey
---

{% assign settings = site.data.sitetext %}

{% include nav.html %}

<style>
  .jitsi-embed {

    height: 92vh;
    width: 100%;
    border: 0px;
    margin-top: 40px;
  }

  @media(min-width:992px) {
    .jitsi-embed {
      height: 72vh;
      margin-top: 85px;
    }
  }

  .wrapper {
    position: relative;
    display: flex;
    align-items: center;
    justify-content: center;
  }

  .anim_loading--ring {
    position: absolute;
    z-index: -1;
  }
</style>

<div class="wrapper">

  <div class="anim_loading--ring">
    <div></div>
    <div></div>
    <div></div>
    <div></div>
  </div>

  <iframe class="jitsi-embed" allow="
    {%- if settings.meeting.camera == 'yes' or settings.meeting.camera == nil -%} camera {%- endif -%};
    {%- if settings.meeting.microphone == 'yes' or settings.meeting.microphone == nil -%} microphone {%- endif -%};
    {%- if settings.meeting.fullscreen == 'yes' or settings.meeting.fullscreen == nil -%} fullscreen {%- endif -%};
    {%- if settings.meeting.display-capture == 'yes' or settings.meeting.display-capture == nil -%} display-capture {%- endif -%};
  " src="{{ settings.meeting.instance }}/{{ settings.meeting.name }}"></iframe>

</div>



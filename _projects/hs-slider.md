---
slug: hs-slider
categories: coding
date: 2021-02-19

# what displays in the portfolio grid
position: 1
caption:
  title: HearthStone Slider
  subtitle: component development
  thumbnail: /assets/img/projects/hs-slider/thumbnail.png

# main content
title: Copying a slider from another website

summary:
  demo: https://riservato-xyz.frama.io/slider-hs

slider:
  - thumbnail: /assets/img/projects/hs-slider/screenshot.png
    title: a fancy slider...

timeline:
    section: timeline
    # left is the default
    #start_align: "left"
    events:
      - title: "Where it all started"
        year: "Feb 2021"
        desc: "We were first approached on [99Freelas](https://www.99freelas.com.br/project/extrair-ou-criar-slider-parecido-238630){:target='_blank'} platform, where we sent a proof of concept to the client, even before getting our offer approved by them."
        image: 
          src: /assets/img/projects/hs-slider/approach.png
          art_direction: "background-position: -3rem; background-size: 180%;"
        alt: 
      - title: "First Things First"
        year: "Feb 2021"
        desc: "Right after getting our offer approved by the client, we started downloading assets from the official [HearthStone website](https://playhearthstone.com/pt-br){:target='_blank'}."
        image: 
          src: /assets/img/projects/hs-slider/inspector.jpg
          art_direction: "background-position: 0 -17rem; background-size: 630%;"
        alt: 
      - title: "Slider library"
        year: "Feb 2021"
        desc: "We ended up making development with [Bootstrap carousel component](https://getbootstrap.com/docs/5.0/components/carousel/){:target='_blank'}, since it allows customizing indicators with ease. A perfect match for our client needs!"
        image: 
          src: /assets/img/general/bootstrap.svg
          art_direction: "background-color: #7952b3; background-size: 70%;"
        alt: image alt text
      - title: "Development"
        year: "Feb 2021"
        desc: "It was just a matter of setting up Bootstrap carousel basic HTML structure, resetting some styles and then copying those fancy styles that makes it a beautiful slider. **NOT!**"
        image: 
          src: /assets/img/general/dev.png
        alt: image alt text
        alt: image alt text
      - title: "Modularizing CSS"
        year: "Feb 2021"
        desc: "Other than just delivering a copy of someone else's website, we went a little bit further and decided to make CSS class names using [BEM](http://getbem.com/naming/){:target='_blank'} naming convention."
        image: 
          src: /assets/img/general/bem.svg
          art_direction: "background-color: white; background-size: 70%;"
        alt: image alt text
      - title: "Color variation"
        year: "Feb 2021"
        desc: "Thanks to [BEM](http://getbem.com/naming/){:target='_blank'}, we were able to develop components that has varying colors. Our client can easily switch between 5 different button and indicator countdown colors."
        image: 
          src: /assets/img/general/bem.svg
          art_direction: "background-color: white; background-size: 70%;"
        alt: image alt text
      - title: "Sass Way"
        year: "Feb 2021"
        desc: "It's not only easy to develop using Sass language, but also: rather easy to maintain code and implement [BEM](http://getbem.com/naming/){:target='_blank'} naming convention."
        image: 
          src: /assets/img/general/sass.png
          art_direction: "background-size: 10rem;"
        alt: image alt text
      - title: "Sane JavaScript"
        year: "Feb 2021"
        desc: "As opposed to delivering tangled code, we used OOP concepts to make sure code would be clean, organized and allow easy configuration."
        image: 
          src: /assets/img/general/javascript.jpg
          art_direction: "background-position: -0.3rem;"
      - title: "Jekyll Integration"
        year: "Feb 2021"
        desc: "The whole source code is integrated with [Jekyll](https://jekyllrb.com/){:target='_blank'}. It allows easy generation of slides, buttons, content and JS configuration."
        image: 
          src: /assets/img/general/jekyll.png
      - title: "Payment"
        year: "Feb 2021"
        desc: "After everything was delivered, we were paid by the client."
        image: 
          src: /assets/img/general/review.jpg
        alt: 
    end: "Be Part <br> of Our <br> Story!"
---

{:.section-subheading.text-muted}
Our customer from [**On Air Publicidade E Marketing Digital**][onair]{:target='_blank'} requested services for extracting or copying a slider from a [HearthStone website][hs]{:target='_blank'}. The extracting option was early discarded, since it would require much more time to remove bloating code, as well as refactoring obfuscated code. We ended up copying styles with the help of [Firefox Page Inspector][inspector] and delivering an easy to mainting code.

[hs]: https://playhearthstone.com/pt-br
[onair]: https://www.onairpublicidadedigital.com/
[inspector]: https://developer.mozilla.org/en-US/docs/Tools/Storage_Inspector

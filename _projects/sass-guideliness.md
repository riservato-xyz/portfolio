---
slug: sass guidelines
categories: translation
date: 2018-02-25

# what displays in the portfolio grid
caption:
  title: Sass Guidelines
  subtitle: technical documentation
  thumbnail: /assets/img/projects/sass-guidelines/thumbnail.png

# main content
title: You scratch my back and I’ll scratch yours

summary:
  demo: https://sass-guidelin.es/pt

slider:
  - thumbnail: /assets/img/projects/sass-guidelines/shame.before.jpg
    title: given text...
  - thumbnail: /assets/img/projects/sass-guidelines/shame.after.jpg
    title: produced translation...

timeline:
    section: timeline
    # left is the default
    #start_align: "left"
    events:
      - title: "Where it all started"
        year: "June 2018"
        desc: "We first met on [GitHub](https://github.com/KittyGiraudel/sass-guidelines){:target='_blank'}."
        image: 
          src: /assets/img/general/github.png
          art_direction: "background-size: 100%;"
        alt: 
      - title: "Documentation"
        year: "June 2018"
        desc: "We got started by reading the project's documentation on how to translate the website, properly."
        image: 
          src: /assets/img/projects/sass-guidelines/docs.png
          art_direction: "background-position: -2rem 0; background-size: 380%;"
        alt: 
      - title: "First lines"
        year: "June 2018"
        translated: true
        desc: 
          - "My name is Kitty Giraudel, I am a French front-end developer, based in Berlin (Germany) since 2015, currently working at Gorillas."
          - "O meu nome é Kitty Giraudel, sou um front-end francês, morando em Berlim (Alemanha) desde 2015 e, atualmente, trabalhando na Gorillas."
        image: 
          src: /assets/img/projects/sass-guidelines/author.png
          art_direction: "background-position: 0 0;background-size: 220%;"
        alt: image alt text
      - title: "Outcome"
        year: "June 2018"
        desc: "After translating 15,000+ words, we had all of the documentation translated and available for Portuguese language readers."
        image:
          src: /assets/img/projects/sass-guidelines/outcome.png
          art_direction: "background-size: 210%; background-position: -90px -260px;"
        alt: 
    end: "Be Part <br> of Our <br> Story!"
---

{:.section-subheading.text-muted}
**Sass Guidelines** is an opinionated styleguide for writing sane, maintainable and scalable Sass. This is an important project for the community of web developers, since it heps to establish good practices for developing websites. Although, it was lacking a translation for Portuguese language and that is when we got into helping them.

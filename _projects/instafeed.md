---
slug: instafeed
categories: 
  - design
date: 2019-02-25

# what displays in the portfolio grid
caption:
  title: InstaFeed
  subtitle: landing page
  thumbnail: /assets/img/projects/instafeed/thumbnail.png

# main content
title: Stylish landing page for Instagram feed

slider:
  - thumbnail: /assets/img/projects/instafeed/closed-laptop-and-smartphone.png
    title: neat & responsive
  - thumbnail: /assets/img/projects/instafeed/responsive.png
    title: bigger screens?
  - thumbnail: /assets/img/projects/instafeed/devices-plant.png
    title: I really mean it...

timeline:
    section: timeline
    # left is the default
    #start_align: "left"
    events:
      - title: "Where it all started"
        year: "Feb 2019"
        desc: "We were first approached on [99Freelas](https://www.99freelas.com.br/user/jlammer){:target='_blank'} platform, where we had our first briefing."
        image: 
          src: /assets/img/general/99freelas.png
        alt: 
      - title: "Meeting"
        year: "Feb 2019"
        desc: "Right after we had most stuff settled using 99Freelas chat, we decided to sit and have a conversation in order to put details into paper."
        image: 
          src: /assets/img/general/meeting.png
        alt: 
      - title: "Design"
        year: "Feb 2019"
        desc: "Once we knew we were at the same page, we started working by designing the landing page using Photoshop with **mobile first principle** in mind."
        image: 
          src: /assets/img/projects/instafeed/breakpoints.png
        alt: image alt text
      - title: "Development"
        year: "Feb 2019"
        desc: "After that, it was just a matter of implementing the existing **responsive design** into code."
        image: 
          src: /assets/img/general/dev.png
        alt: 
      - title: "Payment"
        year: "Feb 2019"
        desc: "After everything was delivered, we were paid by the client."
        image: 
          src: /assets/img/general/review.jpg
        alt: 
    end: "Be Part <br> of Our <br> Story!"
---

{:.section-subheading.text-muted}
Our customer from **InstaFeed** requested services for a landing page design and one of the requirements was: **responsive design**. Everything worked out nicely, **within deadline** and in the end we had a stylish landing page ready for use on Wordpress.

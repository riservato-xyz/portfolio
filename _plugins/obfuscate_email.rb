module Jekyll
  module EmailObfuscationFilter
    def encode_email_char(char)
      encoded_chars = [
        "&#"  + char.ord.to_s     + ";",
        "&#x" + char.ord.to_s(16) + ";",
                char,
      ]
    
      # This must be encoded
      if char == "@"
        encoded_chars[0..1].sample
      else
        r = rand()
        if r > 0.9
          encoded_chars[2]
        elsif r < 0.45
          encoded_chars[1]
        else
          encoded_chars[0]
        end
      end
    end

    def encode_email(addr)
      addr
        .chars.map { |char| encode_email_char(char) }
        .join("")
    end
  end
end

Liquid::Template.register_filter(Jekyll::EmailObfuscationFilter)

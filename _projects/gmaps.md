---
slug: gmaps
categories: coding
date: 2019-02-25

# what displays in the portfolio grid
position: 2
caption:
  title: GMaps
  subtitle: Google Maps tool
  thumbnail: /assets/img/projects/gmaps/thumbnail.png

# main content
title: Google Maps embed generation tool

summary:
  demo: https://gmaps.com.br

slider:
  - thumbnail: /assets/img/projects/gmaps/home.png
    title: clean and beautiful view
  - thumbnail: /assets/img/projects/gmaps/responsive.png
    title: responsive design
  - thumbnail: /assets/img/projects/gmaps/testimonials.png
    title: testimonials section
  - thumbnail: /assets/img/projects/gmaps/blog.png
    title: ready to use blog

timeline:
    section: timeline
    # left is the default
    #start_align: "left"
    events:
      - title: "Where it all started"
        year: "Feb 2019"
        desc: "We were first approached on [99Freelas](https://www.99freelas.com.br/user/jlammer){:target='_blank'} platform, where we had our first briefing."
        image: 
          src: /assets/img/general/99freelas.png
        alt: 
      - title: "Meeting"
        year: "Feb 2019"
        desc: "Right after we had most stuff settled using 99Freelas chat, we decided to sit and have a conversation in order to put details into paper."
        image: 
          src: /assets/img/general/meeting.png
        alt: 
      - title: "Responsive Design"
        year: "Feb 2019"
        desc: "Once we knew we were at the same page, we started working by designing the website using Photoshop with **mobile first principle** in mind."
        image: 
          src: /assets/img/projects/gmaps/devices-plants.png
        alt: image alt text
      - title: "Generated Maps"
        year: "Feb 2019"
        desc: "After code implementation, we have a beautiful tool that generates embedded maps with the website's link."
        image: 
          src: /assets/img/projects/gmaps/map.jpg
        alt: 
      - title: "Payment"
        year: "Feb 2019"
        desc: "After everything was delivered, we were paid by the client."
        image: 
          src: /assets/img/general/review.jpg
        alt: 
    end: "Be Part <br> of Our <br> Story!"
---

{:.section-subheading.text-muted}
Our customer from **GMaps** requested services for development of a tool that would generate maps for given user input. Everything worked out nicely, **within deadline** and we delivered a website entirely integrated with Wordpress.

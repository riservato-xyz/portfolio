---
slug: bootstrap
categories: translation
date: 2018-02-25

# what displays in the portfolio grid
caption:
  title: Bootstrap
  subtitle: technical documentation
  thumbnail: /assets/img/projects/bootstrap/thumbnail.png

# main content
title: You scratch my back and I’ll scratch yours

summary:
  demo: https://getbootstrap.com.br/docs/4.1/getting-started/introduction/

slider:
  - thumbnail: /assets/img/projects/bootstrap/introduction.before.jpg
    title: given text...
  - thumbnail: /assets/img/projects/bootstrap/introduction.after.jpg
    title: produced translation...

timeline:
    section: timeline
    # left is the default
    #start_align: "left"
    events:
      - title: "Where it all started"
        year: "June 2018"
        desc: "We first met on [GitHub](https://github.com/bootstrapbrasil/bootstrap){:target='_blank'}."
        image: 
          src: /assets/img/general/github.png
          art_direction: "background-size: 100%;"
        alt: 
      - title: "Documentation"
        year: "June 2018"
        desc: "We got started by reading the project's documentation on how to translate the website, properly."
        image: 
          src: /assets/img/projects/bootstrap/docs.jpg
          art_direction: "background-position: 0; background-size: 380%;"
        alt: 
      - title: "First lines"
        year: "June 2018"
        translated: true
        desc: 
          - "Using color to add meaning only provides a visual indication, which will not be conveyed to users of assistive technologies..."
          - "Usando cor para adicionar significado só fornece uma indicação visual, a qual não será transmitida para usuários..."
        image: 
          src: /assets/img/projects/bootstrap/assitive-tech.after.png
          art_direction: "background-position: -10px center;;background-size: 480%;"
        alt: image alt text
      - title: "Outcome"
        year: "June 2018"
        desc: "After translating 17,000+ words, we had all of the documentation translated and available for Portuguese language readers."
        image:
          src: /assets/img/projects/bootstrap/outcome.png
          art_direction: "background-size: 210%; background-position: 0 0;"
        alt: 
    end: "Be Part <br> of Our <br> Story!"
---

{:.section-subheading.text-muted}
**Bootstrap** is the world’s most popular front-end open source toolkit, featuring Sass variables and mixins, responsive grid system, extensive prebuilt components, and powerful JavaScript plugins. This is an important project for the community of web developers, since it helps to quickly design and customize responsive mobile-first sites. Although, it was lacking a translation for Portuguese language and that is when we got into helping them.

FROM alpine:3.13

RUN apk update \
 && apk upgrade \
 && apk add --no-cache \
            tor --update-cache --repository http://dl-4.alpinelinux.org/alpine/edge/community/ --allow-untrusted \
            bash \
            torsocks \
            rsync \
            openssh-client \
            sshpass \
            ca-certificates \
 && update-ca-certificates \
 && rm -rf /var/cache/apk/*

EXPOSE 9150

ADD ./torrc /etc/tor/torrc
RUN chown -R tor /etc/tor

USER tor

CMD /usr/bin/tor -f /etc/tor/torrc


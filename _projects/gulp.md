---
slug: gulp
categories: translation
date: 2018-02-25

# what displays in the portfolio grid
caption:
  title: Gulp
  subtitle: technical documentation
  thumbnail: /assets/img/projects/gulp/thumbnail.png

# main content
title: You scratch my back and I’ll scratch yours

slider:
  - thumbnail: /assets/img/projects/gulp/files.before.jpg
    title: given text...
  - thumbnail: /assets/img/projects/gulp/files.after.jpg
    title: produced translation...

timeline:
    section: timeline
    # left is the default
    #start_align: "left"
    events:
      - title: "Where it all started"
        year: "November 2020"
        desc: "We first met on [GitHub](https://github.com/gulpjs/gulp/issues/2481){:target='_blank'}."
        image: 
          src: /assets/img/general/github.png
          art_direction: "background-size: 100%;"
        alt: 
      - title: "Documentation"
        year: "November 2020"
        desc: "We got started by reading the project's documentation on how to translate the website, properly."
        image: 
          src: /assets/img/projects/gulp/docs.jpg
          art_direction: "background-position: 0; background-size: 450%%;"
        alt: 
      - title: "First lines"
        year: "November 2020"
        translated: true
        desc: 
          - "Gulp allows you to use existing JavaScript knowledge to write gulpfiles or to use your experience with gulpfiles to write plain JavaScript..."
          - "Gulp permite que você use seu conhecimento em JavaScript para escrever gulpfiles..."
        image: 
          src: /assets/img/projects/gulp/javascript.after.jpg
          art_direction: "background-position: 0 center;;background-size: 670%;"
        alt: image alt text
      - title: "Outcome"
        year: "November 2020"
        desc: "After translating 9,000+ words, we had all of the documentation translated and available for Portuguese language readers."
        image:
          src: /assets/img/projects/gulp/outcome.png
          art_direction: "background-size: 420%; background-position: 5% 0;"
        alt: 
    end: "Be Part <br> of Our <br> Story!"
---

{:.section-subheading.text-muted}
**Gulp** is toolkit to automate and enhance applications development workflow. This is an important project for the community of web developers, since it helps to automate slow, repetitive workflows and compose them into efficient build pipelines. Although, it was lacking a translation for Portuguese language and that is when we got into helping them.

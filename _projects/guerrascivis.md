---
slug: guerrascivis
categories: coding
date: 2018-02-25

# what displays in the portfolio grid
position: 4
caption:
  title: Guerras Civis
  subtitle: RPG gaming tool
  thumbnail: /assets/img/projects/guerrascivis/thumbnail.png

# main content
title: Tool for shuffling RPG characters

summary:
  demo: https://hunkjazz.github.io/guerrascivis

slider:
  - thumbnail: /assets/img/projects/guerrascivis/roles.jpg
    title: roles selection
  - thumbnail: /assets/img/projects/guerrascivis/output.png
    title: output table
  - thumbnail: /assets/img/projects/guerrascivis/help.png
    title: help lightbox

timeline:
    section: timeline
    # left is the default
    #start_align: "left"
    events:
      - title: "Where it all started"
        year: "June 2018"
        desc: "We first met on [GitHub](https://github.com/hunkjazz/guerrascivis/issues){:target='_blank'}."
        image: 
          src: /assets/img/general/github.png
          art_direction: "background-size: 100%;"
        alt:  
      - title: "Meeting"
        year: "Feb 2018"
        desc: "Right after we had most stuff settled by text messages, we decided to sit and have a conversation in order to put details into paper."
        image: 
          src: /assets/img/general/meeting.png
        alt: 
      - title: "Responsive Design"
        year: "Feb 2018"
        desc: "Once we knew we were at the same page, we started working by designing the website using Photoshop with **mobile first principle** in mind."
        image: 
          src: /assets/img/projects/guerrascivis/mobile.png
          art_direction: "background-position-y: 0;"
        alt: image alt text
      - title: "Development"
        year: "Feb 2019"
        desc: "After that, it was just a matter of implementing the existing **responsive design** into code."
        image: 
          src: /assets/img/general/dev.png
        alt: 
      - title: "Deployment"
        year: "Feb 2019"
        desc: "We have setup CI/CD using GitHub pages. In this way, the website wouldn't need a private host or anything. All for free, thanks to GitHub."
        image: 
          src: /assets/img/projects/guerrascivis/deploy.png
          art_direction: "background-position: 0 0;"
        alt: 
      - title: "Payment"
        year: "Feb 2018"
        desc: "After everything was delivered, we were paid by the client."
        image: 
          src: /assets/img/general/review.jpg
        alt: 
    end: "Be Part <br> of Our <br> Story!"
---

{:.section-subheading.text-muted}
Our customer from **Guerras Civis** requested services for development of a tool that would shuffle RPG roles with given input name. Everything worked out nicely, **within deadline** and we delivered a website entirely integrated with Wordpress.

---
slug: pz solucoes
categories: coding
date: 2019-02-25

# what displays in the portfolio grid
caption:
  title: PZ Soluções
  subtitle: agency website
  thumbnail: /assets/img/projects/pz-solucoes/thumbnail.jpg

# main content
title: Can we have a responsive website?

summary:
  demo: https://pzsolucoes.com.br

slider:
  - thumbnail: /assets/img/projects/pz-solucoes/phone.png
    title: neat & responsive
  - thumbnail: /assets/img/projects/pz-solucoes/devices-plant.png
    title: fits any device

timeline:
    section: timeline
    # left is the default
    #start_align: "left"
    events:
      - title: "Where it all started"
        year: "Feb 2019"
        desc: "We were first approached on [99Freelas](https://www.99freelas.com.br/user/jlammer){:target='_blank'} platform, where we had our first briefing."
        image: 
          src: /assets/img/general/99freelas.png
        alt: 
      - title: "Meeting"
        year: "Feb 2019"
        desc: "Right after we had most stuff settled using 99Freelas chat, we decided to sit and have a conversation in order to put details into paper."
        image: 
          src: /assets/img/general/meeting.png
        alt: 
      - title: "Code Review"
        year: "Feb 2019"
        desc: "Once we knew we were at the same page, we started working by reading and analysing the website code."
        image: 
          src: /assets/img/projects/pz-solucoes/review.png
        alt: image alt text
      - title: "Code Refactoring"
        year: "Feb 2019"
        desc: "Adapting existing code was no longer an option. We put it aside, kept brand design and started coding everything from ground zero."
        image: 
          src: /assets/img/general/dev.png
        alt: 
      - title: "Features Implementation"
        year: "Feb 2019"
        desc: "Since our client was a lot happy with the outcome so far, they have decided to request development of a feature that would make it easier for clients to contact them."
        image: 
          src: /assets/img/projects/pz-solucoes/feature.png
        alt: 
      - title: "Payment"
        year: "Feb 2019"
        desc: "After everything was delivered, we were paid by the client."
        image: 
          src: /assets/img/general/review.jpg
        alt: 
    end: "Be Part <br> of Our <br> Story!"
---

{:.section-subheading.text-muted}
Our customer from **PZ Soluções** requested services for layout refactoring, so their website would become responsive and fit any device screen, no matter what size it is. It was a rather tricky process, since the website was not made with _mobile first_ design principle in mind. Hence, our idea of just adapting existing code had to be discarded, since it would not be worth the time. In the end, we had the whole code refactored and some contact information features implemented.
